.. Track Analyzer - Quantification and visualization of tracking data
    Authors: Arthur Michaut                                                
    Copyright 2016-2019 Harvard Medical School and Brigham and             
                             Women's Hospital                              
    Copyright 2019-2024 Institut Pasteur and CNRS–UMR3738                  
    See the COPYRIGHT file for details                                     
                                                                           
    This file is part of Track Analyzer package.                           
                                                                           
    Track Analyzer is free software: you can redistribute it and/or modify 
    it under the terms of the GNU General Public License as published by   
    the Free Software Foundation, either version 3 of the License, or      
    (at your option) any later version.                                    
                                                                           
    Track Analyzer is distributed in the hope that it will be useful,      
    but WITHOUT ANY WARRANTY; without even the implied warranty of         
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the           
    GNU General Public License for more details .                          
                                                                           
    You should have received a copy of the GNU General Public License      
    along with Track Analyzer (COPYING).                                   
    If not, see <https://www.gnu.org/licenses/>.     

************
Installation
************


===========================
Track Analyzer installation
===========================

.. _installation:

- If you haven't installed Python yet, install Python 3.10: download miniconda 3.10: https://docs.conda.io/en/latest/miniconda.html
- Open a Terminal (Mac & Linux) or open an Anaconda powershell (Windows)
- Create environment: run :code:`conda create -y -n TA_env -c conda-forge python=3.10`
- Activate environment (to be run every time you open a new terminal): run :code:`conda activate TA_env`

**Track Analyzer** depends on `napari <https://napari.org/>`_, whose installation can be tricky. For this reason, we recommend to install it independently using conda before installing **Track Analyzer** using pip. ::

    conda install -c conda-forge napari pyqt
    pip install track-analyzer


Archive overview
================

* **doc** => the documentation in html and pdf
* **example** => synthetic data examples are stored in the `example` folder. Each example contains a `params.csv` file containing for each track the "true" values.
* **track_analyzer** => the track analyzer python library
* **setup.py** => the installation script
* **requirements.txt** => the python dependencies
* **COPYING** => the licensing
* **README.md** => brief overview
* **run_TA.ipynb** => the jupyter notebook used for the graphical interface


===========================
Track Analyzer dependencies
===========================

Track Analyzer depends on several python packages that will be installed with the pip install:

- numpy 
- matplotlib
- pandas
- scipy
- scikit-image
- seaborn
- jupyter
- joblib 
- lmfit
- tifffile
- napari
- ipyfilechooser
- ipywidgets
- tqdm
- shapely
