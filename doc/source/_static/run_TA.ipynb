{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Welcome to Track Analyzer!\n",
    "Track Analyzer is a Python-based data visualization pipeline for tracking data. You can find all the necessary documentation: https://track-analyzer.pages.pasteur.fr/track-analyzer/\n",
    "\n",
    "This notebook allows you to run the pipeline step by step. It comprises 5 main modules. The first 2 modules need to be run successively before any analyses can be done: \n",
    "- the **preparation module** allows you to load the data and fill in the necessary metadata\n",
    "- the **filtering module** allows you to filter your data into one or several datasets (which can be compared together)\n",
    "\n",
    "Then, 3 analysis modules can be used independently of each other: \n",
    "- the **trajectory analysis module** which aims at visualizing and quantifying trajectories\n",
    "- the **map analysis module** that averages several parameters on a regular grid to produce maps\n",
    "- the **comparator module** that compares analyses previously produced by the trajectory analysis module. \n",
    "\n",
    "For each module, please run **all** cells successively in order. If the program raises an error, please verify that you haven't forgotten to run a cell. \n",
    "\n",
    "Don't hesitate to report any issue on the project repository: https://gitlab.pasteur.fr/track-analyzer/track-analyzer\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import TrackAnalyzer and other necessary packages\n",
    "\n",
    "import os\n",
    "import os.path as osp\n",
    "from skimage import io\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "import warnings\n",
    "\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import HBox, VBox, interact, interact_manual, GridspecLayout, Label, AppLayout, Layout\n",
    "from ipyfilechooser import FileChooser\n",
    "from IPython.display import Markdown, display\n",
    "\n",
    "from track_analyzer import prepare as tpr\n",
    "from track_analyzer import plotting as tpl\n",
    "from track_analyzer import notebook as tnb\n",
    "from track_analyzer.scripts.analyze_tracks import traj_analysis\n",
    "from track_analyzer.scripts.analyze_maps import map_analysis\n",
    "from track_analyzer.scripts.compare_datasets import compare_datasets\n",
    "\n",
    "from napari.settings import get_settings\n",
    "get_settings().application.ipy_interactive = False  # disable interactive usage of Napari viewer (necessary for tpr.get_coordinates)\n",
    "\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "%matplotlib inline\n",
    "\n",
    "def printmd(string):\n",
    "    display(Markdown(string))\n",
    "\n",
    "cwd = os.getcwd()  # working directory\n",
    "plot_param = tpl.make_plot_config()  # some config parameters\n",
    "color_list = plot_param['color_list']  # a list of colors often used\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Preparation module\n",
    "\n",
    "In this module, you will load the data, fill in the required metadata and customize your plotting configuration. \n",
    "\n",
    "## Load your data\n",
    "Load the file containing the tracks and specify how it is formatted. \n",
    "If you have previously loaded this dataset, you can directly reload it without specify the metadata, just run all the cells of the module. \n",
    "You can optionally load the original movie to plot your trajectories on it. It needs to be tif stack. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#choose positions file\n",
    "fc_table = FileChooser(cwd)\n",
    "fc_table.use_dir_icons = True\n",
    "fc_table.title = '<b>Tracking data file</b>'\n",
    "\n",
    "sep_wid = widgets.Dropdown(options=[',',';', 'tab', ' '],value=',',description='column separator:',style={'description_width': 'initial'})\n",
    "header_wid = widgets.Checkbox(value=True, description='First row = column names')\n",
    "trackmate_wid = widgets.Checkbox(value=True, description='Trackmate (version 7) csv file')\n",
    "\n",
    "printmd(\"\"\"**Browse your file system to the table of tracked data (only .txt and .csv are supported)**\"\"\")\n",
    "\n",
    "display(fc_table)\n",
    "\n",
    "printmd(\"\"\"**Give information about the input file format**\"\"\")\n",
    "\n",
    "display(sep_wid, header_wid, trackmate_wid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get position file path\n",
    "data_dir = fc_table.selected_path\n",
    "data_file = fc_table.selected\n",
    "\n",
    "if data_file is None: \n",
    "    raise Exception(\"**ERROR: no data table has been selected**\")\n",
    "\n",
    "# choose image file\n",
    "printmd(\"\"\"**(Optional) Browse your file system to the image file**  \n",
    "        You can plot your data on your image. The image can be a single image or a stack (a 2D time series or a 3D time series). \n",
    "        Only tif images are supported. \"\"\")\n",
    "\n",
    "fc_im = FileChooser(data_dir)\n",
    "fc_im.use_dir_icons = True\n",
    "fc_im.title = '<b>Image file</b>'\n",
    "\n",
    "display(fc_im)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get image file path\n",
    "im_file = fc_im.selected\n",
    "\n",
    "# analyze image\n",
    "image,y_size,x_size,check_swap_wid,swap_wid = tnb.analyze_image(data_dir,filename=im_file,verbose=False)\n",
    "\n",
    "# refresh database and info if needed\n",
    "database_fn=osp.join(data_dir,'data_base.p')\n",
    "info_fn=osp.join(data_dir,'info.txt')\n",
    "printmd(\"---\")\n",
    "if osp.exists(database_fn):\n",
    "    printmd('The database already exists, do you want to refresh it?')\n",
    "    refresh_db_wid=widgets.ToggleButton(value=False,description='Refresh database')\n",
    "    display(refresh_db_wid)\n",
    "if osp.exists(info_fn):   \n",
    "    printmd(\"The info.txt file already exists, do you want to refresh it?\")\n",
    "    refresh_info_wid=widgets.ToggleButton(value=False,description='Refresh info')\n",
    "    display(refresh_info_wid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "#swap z and t\n",
    "if check_swap_wid:\n",
    "    if swap_wid.value:\n",
    "        t_dim = image['t_dim']\n",
    "        z_dim = image['z_dim']\n",
    "        image['t_dim'] = z_dim\n",
    "        image['z_dim'] = t_dim\n",
    "        printmd(\"**z and t swapped!**\")\n",
    "        im = io.imread(image['image_fn'])\n",
    "        printmd(\"4D image with {} time steps and {} z slices\".format(im.shape[image['t_dim']],im.shape[image['z_dim']]))\n",
    "        del im  # free memory \n",
    "\n",
    "# retrieve refresh widgets values\n",
    "refresh_db = refresh_db_wid.value if osp.exists(database_fn) else True\n",
    "refresh_info = refresh_info_wid.value if osp.exists(info_fn) else True\n",
    "\n",
    "# get info\n",
    "wid_list,param_names = tnb.make_info_widget(data_dir,y_size,x_size,trackmate_wid.value)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "# if refreshing info, save info as txt file\n",
    "if refresh_info:\n",
    "    info = {}\n",
    "    with open(info_fn,'w+') as f:\n",
    "        for couple in zip(param_names,wid_list):\n",
    "            info[couple[0]]=couple[1].value\n",
    "            f.write('{}:{}\\n'.format(couple[0],couple[1].value))\n",
    "else: \n",
    "    info = tpr.get_info(data_dir)\n",
    "\n",
    "# if no image, get image size from the info file\n",
    "if im_file is None:\n",
    "    image = tpr.get_image(data_dir,filename=im_file,verbose=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# if refreshing database, select variables to keep\n",
    "if refresh_db:\n",
    "    df,col_wid_list,var_wid_dict,split_wid,var_list = tnb.make_database_widget(data_file,sep_wid, header_wid, trackmate_wid.value)\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# populate database if refreshing it or if it does not exist\n",
    "if refresh_db:\n",
    "    # reorganize df\n",
    "    df,custom_var,split_traj,dim_list = tnb.reorganize_df(df,col_wid_list,var_wid_dict,split_wid,var_list)\n",
    "    \n",
    "    # coordinates origin and signs\n",
    "    ori_onimage_wid,reset_dim_wid,origin_coord_wid_list,invert_axes_wid = tnb.make_coord_widget(df,dim_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# save database or reload it\n",
    "if refresh_db:    \n",
    "    # get origin coordinates\n",
    "    set_origin_ = tnb.unpack_origin_coord(ori_onimage_wid,origin_coord_wid_list,dim_list)\n",
    "    \n",
    "    printmd(\"Processing data (this step can be slow for large database)\")\n",
    "    data = tpr.get_data(data_dir,df=df,refresh=refresh_db,split_traj=split_traj,\n",
    "                        set_origin_=set_origin_,image=image,reset_dim=reset_dim_wid.value,\n",
    "                        invert_axes=invert_axes_wid.value,custom_var=custom_var)\n",
    "else: \n",
    "    # reload from database\n",
    "    data = tpr.get_data(data_dir,df=None,refresh=refresh_db)\n",
    "\n",
    "# useful variables\n",
    "df = data['df']\n",
    "lengthscale = data['lengthscale']\n",
    "timescale = data['timescale']\n",
    "dim = data['dim']\n",
    "dimensions = data['dimensions']\n",
    "custom_var = data['custom_var']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# general plotting configuration\n",
    "fig_w_wid,fig_h_wid,fig_dpi_wid,fig_resfac_wid,fig_format_wid,save_as_stack_wid,despine_wid,replace_color_wid,add_replace_wid,invert_yaxis_wid,export_data_pts_wid = tnb.make_plotconfig_widget(plot_param)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# replace colors \n",
    "color_wid_list=[]\n",
    "for i in range(replace_color_wid.value):\n",
    "    color_wid=widgets.ColorPicker(description='Pick color #{}'.format(i),value=color_list[i])\n",
    "    color_wid_list.append(color_wid)\n",
    "display(*color_wid_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# retreive plotting configuration\n",
    "color_list_=list(color_list)\n",
    "new_color_list = [color_wid_list[i].value for i in range(replace_color_wid.value)]\n",
    "\n",
    "if add_replace_wid.value=='replace':\n",
    "    color_list_[:replace_color_wid.value]=new_color_list\n",
    "else: \n",
    "    color_list_=new_color_list+color_list_\n",
    "    \n",
    "plot_param={'figsize':(fig_w_wid.value,fig_h_wid.value),\n",
    "            'dpi':fig_dpi_wid.value,\n",
    "            'figsize_factor':fig_resfac_wid.value,\n",
    "            'color_list':color_list_,\n",
    "            'format':fig_format_wid.value,\n",
    "            'despine':despine_wid.value,\n",
    "            'invert_yaxis':invert_yaxis_wid.value, \n",
    "            'export_data_pts':export_data_pts_wid.value,\n",
    "            'save_as_stack': save_as_stack_wid.value,\n",
    "           }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Filtering module\n",
    "This module allows you to filter your data based on several criteria: \n",
    "- the positions in space and time\n",
    "- the trajectories duration\n",
    "\n",
    "You can also select a subset of trajectories based on their position at a specific time point to be able to inspect their history and future. \n",
    "\n",
    "All these filters can be used to produced one or several datasets that can be analyzed independently or in comparison. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# inspect trajectories with a napari viewer\n",
    "\n",
    "printmd('**View trajectories on a Napari viewer before plotting**')\n",
    "printmd('If not working, please check Napari installation: https://napari.org/')\n",
    "printmd('Warning: large image can lead to rendering issues')\n",
    "\n",
    "z_step = info['z_step']\n",
    "if z_step == 0:\n",
    "    z_step = lengthscale  # if z_step not given, same as lengthscale\n",
    "\n",
    "if image['z_dim'] is None: \n",
    "    printmd('Warning: you have a 2D+t image, no 3D redering is available then.')\n",
    "\n",
    "viewer_wid = widgets.Button(value=True, description='Show viewer!')\n",
    "\n",
    "viewer_wid.on_click(lambda obj: tpl.view_traj(df, image=image, z_step=info['z_step']))\n",
    "\n",
    "display(viewer_wid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# filter data, in different subsets if needed\n",
    "\n",
    "printmd(\"Your data can be filtered into subsets. How many subsets do you want to analyze?\")\n",
    "subset_num_wid = widgets.BoundedIntText(value=1,min=1,max=10,description='Number of subsets:',style={'description_width': 'initial'})\n",
    "display(subset_num_wid)\n",
    "\n",
    "printmd(\"If you define several subsets, do you want to analyze them separately or together?\")\n",
    "printmd(\"If analyzed together, subsets will be plotted together. If analyzed separately, each subset will be plotted on individual plots.\")\n",
    "separate_widget = widgets.Dropdown(options=['separately', 'together'], value='separately', description='Analyze subsets',style={'description_width': 'initial'})\n",
    "display(separate_widget)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# for each subset, use widgets to filter data\n",
    "filter_wid_dict = tnb.make_filter_widget(df,subset_num_wid.value,image)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# unpack filter values and select sets of tracks\n",
    "filters, set_num_wid = tnb.retrieve_filter_values(subset_num_wid.value, filter_wid_dict)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use widgets to select set of tracks \n",
    "set_filter_wid_dict = tnb.make_track_widget(set_num_wid.value,df,image)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# unpack widgets values and give names to subests\n",
    "new_filters, filt_names_wid_list, order_wid_list = tnb.retrieve_set_filter_values(filters,set_num_wid.value, set_filter_wid_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# store all filters\n",
    "order_list = []  # list for subsets indices\n",
    "for i in range(len(new_filters)):\n",
    "    new_filters[i]['name'] = filt_names_wid_list[i].value\n",
    "    order_list.append(order_wid_list[i].value - 1)  # using numbering starting at 1 for users \n",
    "if any(x not in order_list for x in range(len(new_filters))):  # check if all indices have been set     \n",
    "    print(\"Warning: the ordering is wrong. Reverting to default order.\")\n",
    "    order_list = [x for x in range(len(new_filters))]\n",
    "    \n",
    "order_list_ = [new_filters[i]['name'] for i in order_list]  # order_list of names\n",
    "\n",
    "filters_dict = {'subset': separate_widget.value, \n",
    "               'filters_list': new_filters,\n",
    "               'subset_order': order_list_,}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true
   },
   "source": [
    "# Trajectory analysis module\n",
    "This module analyses tracjectory-based parameters: velocity, acceleration, Mean Squared Displacement (MSD), the local density (by Voronoi tesselation). \n",
    "\n",
    "The different quantities can be visualized using color code on the trajectories or plotted together using histograms, boxplots and scatter plots. \n",
    "\n",
    "All outputs will be saved in a subdirectory of the data directory named: *traj_analysis/<number_of_the_analysis>*. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# make widgets for trajectory analysis\n",
    "traj_wid_dict = tnb.make_traj_param_widgets(data,filters_dict,custom_var,info)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# if the parameters plotting module is selected, make complementary widgets\n",
    "param_plot_widgets_dict = tnb.make_param_plot_widgets(data,traj_wid_dict,custom_var)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# retrieve widgets values to make trajectory configuration\n",
    "traj_config = tnb.retrieve_traj_param_widgets(traj_wid_dict,param_plot_widgets_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# run this cell to analyze trajectories\n",
    "printmd(\"**Running the trajectory analysis!**\")\n",
    "\n",
    "df_list = traj_analysis(data_dir=data_dir,\n",
    "                        data=data,\n",
    "                        image=image,\n",
    "                        filters=filters_dict,\n",
    "                        plot_config=plot_param,\n",
    "                        traj_config=traj_config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Map analysis module\n",
    "\n",
    "This module averages velocity-based parameters (velocity, acceleration, divergence and curl) on a regular grid. \n",
    "\n",
    "You can define the resolution of the grid and how to average data. \n",
    "\n",
    "All outputs will be saved in a subdirectory of the data directory named: *map_analysis/<number_of_the_analysis>*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# make widgets for map analysis\n",
    "printmd(\"**Plot maps of data calculated over a regular grid (velocity, acceleration, divergence, curl, vector means)**\")\n",
    "map_widgets_dict = tnb.make_map_widgets(data_dir,data,custom_var)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# retrieve widgets values for map analysis\n",
    "map_config = tnb.retrieve_map_param_widgets(map_widgets_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "printmd(\"**RUN THE ANALYSIS HERE!**\")  # no output cell, make it visible\n",
    "\n",
    "fdata=map_analysis(data_dir=data_dir,\n",
    "                data=data,\n",
    "                image=image,\n",
    "                filters=filters_dict,\n",
    "                plot_config=plot_param,\n",
    "                map_config=map_config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparator module\n",
    "### This module allows you to compare datasets you have already analyzed by pooling them together on the same plots\n",
    "You can browse your file system to any dataset (even though they don't belong to the same raw data). However, currently, data conversion from datasets with different units *is not supported*. The units will be the one of the  first selected datasets.  \n",
    "You can compare datasets or just pool them together.  \n",
    "Data will be saved in root folder of the first selected dataset (the folder containing the position file, info.txt, database.p, etc.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "printmd(\"**You can either compare datasets (each one have a specific color on plots) or only pool them together (they all have the same color)**\")\n",
    "plotting_mode_wid = widgets.ToggleButtons(options=['compare','pool'],description='Datasets plotting:',style={'description_width': 'initial'})\n",
    "display(plotting_mode_wid)\n",
    "\n",
    "printmd(\"**Give the number of datasets you want to analyze**\")\n",
    "dataset_num_wid = widgets.BoundedIntText(value=0,min=0,max=10,description='Number of datasets:',style={'description_width': 'initial'})\n",
    "display(dataset_num_wid)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# choose datasets folders\n",
    "init_dir = data_dir if 'data_dir' in locals() else cwd\n",
    "fc_list,name_wid_list = tnb.make_folder_widgets(init_dir,dataset_num_wid.value)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# choose number of analyses to be compared\n",
    "dataset_wid_dict = tnb.make_number_analysis_widgets(dataset_num_wid.value,fc_list=fc_list,name_wid_list=name_wid_list)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# make widgets to define the analysis to be compared\n",
    "compare_dict = tnb.make_comp_widgets(dataset_wid_dict)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get all comparison settings and run the comparison\n",
    "\n",
    "# get parameter values\n",
    "dont_run = False if dataset_wid_dict['param_wid_list'][0].value else True\n",
    "param_couples,track_param_couples,param_hist,track_param_hist,param_box,track_param_box = tnb.retrieve_comp_widgets_values(compare_dict,dont_run=dont_run)\n",
    "\n",
    "df_ = compare_datasets( data_dir = dataset_wid_dict['data_dir'],\n",
    "                        df_list = compare_dict['df_list'],\n",
    "                        track_df_list = compare_dict['track_df_list'],\n",
    "                        MSD_df_list = compare_dict['MSD_df_list'],\n",
    "                        datasets_names = dataset_wid_dict['name_list'],\n",
    "                        plotting_mode = plotting_mode_wid.value,\n",
    "                        param_couples = param_couples,\n",
    "                        param_hist = param_hist,\n",
    "                        param_boxplot = param_box,\n",
    "                        param_track_couples = track_param_couples,\n",
    "                        param_track_hist = track_param_hist,\n",
    "                        param_track_boxplot = track_param_box,\n",
    "                        MSD_plot_param = compare_dict['MSD_plot_param'],\n",
    "                        plot_config = plot_param,\n",
    "                )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
